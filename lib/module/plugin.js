/* eslint-disable */
import * as jsorm from 'jsorm'
import Vue from 'vue'
import { JSORMVue } from 'jsorm-vue'
require('isomorphic-fetch')

Vue.use(JSORMVue)

// Active models
<%= options.models.map(({ model, path }) => `import Create${model}Model from '${path.replace(/\\/g,'/')}'`).join('\n') %>

export default function (ctx, inject) {
  const loggedIn = ctx.app.store.$auth.$state.loggedIn
  const token = ctx.app.store.$auth.$state['token.local']
  const config = (typeof ctx.app.$config.client !== "undefined") ? ctx.app.$config.client : ctx.app.$config
  // Options
  const options = <%= JSON.stringify(options) %>

  let orm = {
    models: {
      <%= options.parentModel %>: Create<%= options.parentModel %>Model(jsorm.JSORMBase, token, config)
    }
  }

  <%=
  options.models.map(({ model, path, base }) => {
    if(base) return ''
    return `// ${model}\n  orm.models.${model} = Create${model}Model(orm.models.${options.parentModel}, jsorm, config)`
  }).join('\n\n  ')
  %>

  inject('orm', orm)
}
